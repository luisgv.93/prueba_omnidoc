//
//  ViewController.swift
//  Prueba_S
//
//  Created by LUIS GOMEZ on 09/06/21.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    
    @IBOutlet weak var tableA:UITableView!
    @IBOutlet weak var txtArtista:UITextField!
    var listaArtistas = [artistDTO]()

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.title = "Busca tu Artista"
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func buscar(){
        if txtArtista.text != nil{
        let txtA = txtArtista.text!.replacingOccurrences(of: " ", with: "%20")
        Request.doRequest.getArtist(artista: txtA) { (response) in
            self.listaArtistas = response
            self.tableA.reloadData()
        }
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaArtistas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "artistCell") as! ArtistTableViewCell
        cell.nombre.text = self.listaArtistas[indexPath.row].nombre
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Request.doRequest.getAlbums(nombreArtista: self.listaArtistas[indexPath.row].nombre, idArtist: self.listaArtistas[indexPath.row].id) { (response ) in
            if response.count > 0{
                let storyboard=UIStoryboard(name: "Main", bundle: nil)
                
                let vcAlbums : ListaAlbumViewController = storyboard.instantiateViewController(withIdentifier: "ListaAlbumViewController") as! ListaAlbumViewController
                vcAlbums.listaAlbums = response
                self.navigationController?.pushViewController(vcAlbums, animated: false)
            }
        }
    }
}

