//
//  ListaAlbumViewController.swift
//  Prueba_S
//
//  Created by LUIS GOMEZ on 10/06/21.
//

import UIKit

class ListaAlbumViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var tablaAlmbum:UITableView!
    
    var listaAlbums=[albumDTO]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.title = "Album´s"
        }
        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaAlbums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "albumCell") as! AlbumTableViewCell
        cell.artista.text = listaAlbums[indexPath.row].artista
        cell.titulo.text = listaAlbums[indexPath.row].nombre
        let url = URL(string: listaAlbums[indexPath.row].imagenRuta)
        if let data = try? Data(contentsOf: url!) {
            cell.imagen.image = UIImage(data: data)
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 114
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Request.doRequest.getTracks(nombreAlbum:listaAlbums[indexPath.row].nombre,nombreArtista:listaAlbums[indexPath.row].artista, idAlbum: listaAlbums[indexPath.row].id) { (response) in
            if response.count > 0{
                let storyboard=UIStoryboard(name: "Main", bundle: nil)
                
                let vcCanciones : ListaCancionesViewController = storyboard.instantiateViewController(withIdentifier: "ListaCancionesViewController") as! ListaCancionesViewController
                vcCanciones.listaCanciones = response
                self.navigationController?.pushViewController(vcCanciones, animated: false)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
