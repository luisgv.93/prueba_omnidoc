//
//  AlbumTableViewCell.swift
//  Prueba_S
//
//  Created by LUIS GOMEZ on 10/06/21.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {
    @IBOutlet weak var titulo:UILabel!
    @IBOutlet weak var artista:UILabel!
    @IBOutlet weak var imagen:UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
