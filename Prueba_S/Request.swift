//
//  Request.swift
//  Prueba_S
//
//  Created by LUIS GOMEZ on 09/06/21.
//

import Foundation
import Alamofire

class Request{
    
    static let doRequest = Request()
    func getToken(completionHandler:@escaping(_ token:String?)->()){
        
        let url = URL(string:"https://accounts.spotify.com/api/token")
        var request = URLRequest(url: url!)
        var components = URLComponents()
        components.queryItems = [
            URLQueryItem(name: "grant_type", value: "client_credentials"),]
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.addValue("Basic MjkyZmJlNzI3ZjY4NGI2ZDk3MDQ2MDFiZTcyOTZhYjA6MDY5OTQ3YWY4Nzc3NGJmZjliMWRmNGNkNThmMzdkZWY=", forHTTPHeaderField: "Authorization")
        request.httpBody = components.query?.data(using: .utf8)
        
        Alamofire.request(request).responseJSON { (response) in
            if !response.result.isFailure{
                
                if let json = response.result.value as? NSDictionary{
                    if let token = json["access_token"] as? String{
                        completionHandler(token)
                    }else{
                        completionHandler(nil)
                    }
                }
            }
        }
        
    }
    func getArtist(artista:String,completionHandler: @escaping(_ artistas:[artistDTO])->()){
        getToken { (token) in
            if token != nil{
                var artistasEncontrados = [artistDTO]()
                let url = URL(string:"https://api.spotify.com/v1/search?q=\(artista)&type=artist")
                var request = URLRequest(url: url!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Bearer \(token!)", forHTTPHeaderField: "Authorization")
                Alamofire.request(request).responseJSON { (response) in
                    if !response.result.isFailure{
                        if let json = response.result.value as? NSDictionary{
                            if let jsonArtist = json["artists"] as? NSDictionary{
                                if let listaArtistas = jsonArtist["items"] as? NSArray{
                                for artists in listaArtistas{
                                    let artist = artists as! NSDictionary
                                    let objArtist = artistDTO()
                                    objArtist.id = artist["id"] as! String
                                    objArtist.nombre = artist["name"] as! String
                                    artistasEncontrados.append(objArtist)
                                }
                                    completionHandler(artistasEncontrados)
                                }
                            }
                            
                        }
                        
                    }
                }
            }
        }
       
    }
    func getAlbums(nombreArtista:String,idArtist:String,completionHandler:@escaping(_ listaAlbums:[albumDTO])->()){
        getToken { (token) in
            if token != nil{
                var listaAlbumsEncontrados=[albumDTO]()
                
                let url = URL(string:"https://api.spotify.com/v1/artists/\(idArtist)/albums")
                var request = URLRequest(url: url!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Bearer \(token!)", forHTTPHeaderField: "Authorization")
                Alamofire.request(request).responseJSON { (response) in
                    if !response.result.isFailure{
                        if let json = response.result.value as? NSDictionary{
                            print(json)
                            if let listaAlbums = json["items"] as? NSArray{
                                for albums in listaAlbums{
                                    let album = albums as! NSDictionary
                                    let objAlbum = albumDTO()
                                    objAlbum.id = album["id"] as! String
                                    objAlbum.nombre = album["name"] as! String
                                    objAlbum.totalCanciones = album["total_tracks"] as! Int
                                    objAlbum.artista = nombreArtista
                                    if let images = album["images"] as? NSArray{
                                        let image = images[0] as! NSDictionary
                                        objAlbum.imagenRuta = image["url"] as! String
                                    }
                                    listaAlbumsEncontrados.append(objAlbum)
                                }
                                completionHandler(listaAlbumsEncontrados)
                            }
                            }
                        }
                }
            }
        }
        
    }
    func getTracks(nombreAlbum:String,nombreArtista:String,idAlbum:String,completionHandler:@escaping(_ listaAlbums:[cancionDTO])->()){
        getToken { (token) in
            if token != nil{
                var listaCancionesEncontrados=[cancionDTO]()
               
                let url = URL(string:"https://api.spotify.com/v1/albums/\(idAlbum)/tracks")
                var request = URLRequest(url: url!)
                request.httpMethod = "GET"
                request.addValue("application/json", forHTTPHeaderField: "Accept")
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("Bearer \(token!)", forHTTPHeaderField: "Authorization")
                Alamofire.request(request).responseJSON { (response) in
                    if !response.result.isFailure{
                        if let json = response.result.value as? NSDictionary{
                            print(json)
                            if let listaCanciones = json["items"] as? NSArray{
                                for canciones in listaCanciones{
                                    let cancion = canciones as! NSDictionary
                                    let objCancion = cancionDTO()
                                    objCancion.nombre = cancion["name"] as! String
                                    objCancion.artista = nombreArtista
                                    objCancion.duracion = cancion["duration_ms"] as! Int
                                    objCancion.album = nombreAlbum
                                    listaCancionesEncontrados.append(objCancion)
                                }
                                completionHandler(listaCancionesEncontrados)
                            }
                        }
                    }
                }
            }
        }
        
    }
}
