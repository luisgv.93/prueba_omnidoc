//
//  DetalleViewController.swift
//  Prueba_S
//
//  Created by LUIS GOMEZ on 10/06/21.
//

import UIKit

class DetalleViewController: UIViewController {
    @IBOutlet weak var titulo:UILabel!
    @IBOutlet weak var album:UILabel!
    @IBOutlet weak var artista:UILabel!
    @IBOutlet weak var duracion:UILabel!
    
    var track=cancionDTO()

    override func viewDidLoad() {
        super.viewDidLoad()
        titulo.text = track.nombre
        artista.text = track.artista
        album.text = track.album
        duracion.text = "Duracion \(track.duracion)"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
