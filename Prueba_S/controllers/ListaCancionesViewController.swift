//
//  ListaCancionesViewController.swift
//  Prueba_S
//
//  Created by LUIS GOMEZ on 10/06/21.
//

import UIKit

class ListaCancionesViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    
    
 
    @IBOutlet weak var tableCacion:UITableView!
    var listaCanciones = [cancionDTO]()
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            self.title = "Canciones"
        }

        // Do any additional setup after loading the view.
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaCanciones.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
        cell.textLabel?.text = listaCanciones[indexPath.row].nombre
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyboard=UIStoryboard(name: "Main", bundle: nil)
        
        let vcDetalle : DetalleViewController = storyboard.instantiateViewController(withIdentifier: "DetalleViewController") as! DetalleViewController
        vcDetalle.track = listaCanciones[indexPath.row]
        self.navigationController?.present(vcDetalle, animated: true, completion: nil)
        
    }
    
   

}
